package ratelimiter

import (
	"fmt"
	"math"
	"testing"
	"time"

	"github.com/go-redis/redis_rate/v10"
	"github.com/redis/go-redis/v9"
	"gitlab.com/dotpe/mindbenders/corel"
	"golang.org/x/net/context"
)

func TestLimiter_Eval(t *testing.T) {

	rl := NewLimiter(redis.NewClient(&redis.Options{Addr: "localhost:6379"}), nil)

	var stopafter = time.Second * 5
	t1 := time.NewTicker(time.Millisecond * 100)
	t2 := time.NewTicker(stopafter)

	type args struct {
		ctx   context.Context
		limit redis_rate.Limit
		key   string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "test-0",
			args: args{
				ctx:   corel.NewOrphanContext(),
				limit: redis_rate.Limit{Period: time.Second * 2, Rate: 5, Burst: 2},
				key:   "testing" + time.Now().Format(time.DateTime),
			},
		},
		{
			name: "test-1",
			args: args{
				ctx:   corel.NewOrphanContext(),
				limit: redis_rate.Limit{Period: time.Second * 4, Rate: 5, Burst: 2},
				key:   "testing" + time.Now().Format(time.DateTime),
			},
		},
	}
	b2i := map[bool]int{false: 0, true: 1}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			count := 0
		L1:
			for {
				select {
				case ts := <-t1.C:
					got := rl.Eval(tt.args.ctx, tt.args.limit, tt.args.key)
					count += b2i[got]
					fmt.Println(ts, got)
				case ts := <-t2.C:
					got := rl.Eval(tt.args.ctx, tt.args.limit, tt.args.key)
					count += b2i[got]
					fmt.Println(ts, got)
					break L1
				}
			}
			estimatedCount := int(float64(tt.args.limit.Rate) * float64(stopafter) / float64(tt.args.limit.Period))
			delta := math.Abs(100 * float64(estimatedCount-(count-tt.args.limit.Burst)) / float64(estimatedCount))
			if delta > 10 {
				t.Errorf(`Failed case( %s : \t(count: %d, estimated: %d, delta:%f  %v`, tt.name, count, estimatedCount, delta, tt.args)
			} else {
				fmt.Printf(`Success case( %s : \t(count: %d, estimated: %d, delta:%f  %v`, tt.name, count, estimatedCount, delta, tt.args)
			}
		})
	}
}
