package ratelimiter

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis_rate/v10"
	"github.com/redis/go-redis/v9"

	"golang.org/x/net/context"
)

const lookupkey = "__rate-limiter"

type Limiter struct {
	limiter   *redis_rate.Limiter
	extractor KeyExtractor
}

func NewLimiter(cl *redis.Client, extractor KeyExtractor) *Limiter {
	if extractor == nil {
		extractor = RLKeyExtractor
	}
	return &Limiter{limiter: redis_rate.NewLimiter(cl), extractor: extractor}
}

// The IsRunnable function sets a value in the gin.Context and calls the next middleware, returning
// whether or not the context was aborted.
// It has to be used along with ExtendedLimiter middlewares only
func IsRunnable(c *gin.Context, resource string) bool {
	c.Set(lookupkey, resource)
	c.Next()
	return !c.IsAborted()
}

// The Eval function checks if a given key has exceeded a specified rate limit using a Redis limiter.
func (l *Limiter) Eval(ctx context.Context, limit redis_rate.Limit, key string) bool {
	key = fmt.Sprintf("%s-%v", key, limit.Period)
	res, err := l.limiter.Allow(ctx, key, limit)
	if err != nil {
		return false
	}
	return res.Allowed+res.Remaining > 0
}

// `ExtendedLimiter` limits the number of requests a user can make using a Redis rate limiter.
// This has to be put after actual Business handler, since it requires input from actual handler.
// The control is passed using c.Next(), which on limit reach inform in c.IsAborted()
func (l *Limiter) ExtendedLeak(limit redis_rate.Limit) gin.HandlerFunc {
	return func(c *gin.Context) {
		if data, ok := c.Get(lookupkey); ok {
			if resource, ok := data.(string); ok {
				if !l.Eval(c, limit, resource) {
					c.Abort()
					c.JSON(http.StatusTooManyRequests, gin.H{"status": false, "message": "too many requests"})
				}
			}
		}
	}
}

// The BasicLimiter function is a middleware that limits the number of requests a client can make based
// on their IP address, HTTP method, and request path.
// This has to be put before actuall business handler, since it doesn't require any input from actual handler
func (l *Limiter) BasicLeak(limit redis_rate.Limit) gin.HandlerFunc {
	return l.BasicLeakWithExtractor(limit, l.extractor)
}

func (l *Limiter) BasicLeakWithExtractor(limit redis_rate.Limit, extr KeyExtractor) gin.HandlerFunc {
	return func(c *gin.Context) {
		if !l.Eval(c, limit, extr(c)) {
			c.Abort()
			c.JSON(http.StatusTooManyRequests, gin.H{"status": false, "message": "too many requests"})
		}
	}
}

type KeyExtractor func(c *gin.Context) string

func RLKeyExtractor(c *gin.Context) string {
	return filepath.Join(
		c.ClientIP(),
		c.Request.Method,
		c.FullPath(),
	)
}

func RLKeyExtractorByUA(c *gin.Context) string {
	return filepath.Join(
		c.ClientIP(),
		c.Request.Method,
		c.FullPath(),
		string(md5.New().Sum([]byte(c.Request.UserAgent()))),
	)
}
